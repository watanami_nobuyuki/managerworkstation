'*********************************************************************
'　マスタ出力サイレントモード
'　端末展開時にご利用ください。
'
'*********************************************************************

FileName = "端末管理台帳.xlsm"
MacroName = "OutputMast"


'★★出力定義名を選択してください。★★
DefName = "端末マスタ"



'現在のパス取得
Dim pwd
Set pwd = Wscript.CreateObject("Scripting.FileSystemObject")
FilePath = pwd.getParentFolderName(WScript.ScriptFullName)
'Excelインスタンス
Dim ExlObj
Set ExlObj = Wscript.CreateObject("Excel.Application")

ExlObj.visible=False
ExlObj.Workbooks.Open FilePath & "\" & FileName
ExlObj.Application.Run MacroName,cstr(DefName)



'★★富士通電子カルテ用-ダンプ出力
Script = "C:\WINDOWS\SYSWOW64\cmd.exe /C ""CScript //Nologo  MDBExporter.vbs C:\egmain-ex\Data\Mdb\ExWorkstation.mdb Base_Workstation C:\egmain-ex\Data\DMP"""
Dir="C:\egmain-ex\Data\DMP"
Set objShell = CreateObject("WScript.Shell")
objShell.currentdirectory=Dir
objShell.Run Script,1,True